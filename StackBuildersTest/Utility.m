//
//  Utility.m
//  SmaatoTest
//
//  Created by Evana Islam on 2/19/16.
//  Copyright © 2016 Evana Islam. All rights reserved.
//

#import "Utility.h"

@implementation Utility

+(void)showAlertWithTitle:(NSString *)title message:(NSString *)msg inView:(UIViewController *)controller{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [controller presentViewController:alertController animated:YES completion:nil];
}

+(NSString *)getStrFromDate:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd.MM.yyyy"];
    NSString *dateStr = [formatter stringFromDate:date];
    return dateStr;
}
@end
