//
//  Utility.h
//  SmaatoTest
//
//  Created by Evana Islam on 2/19/16.
//  Copyright © 2016 Evana Islam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface Utility : NSObject

+(void)showAlertWithTitle:(NSString *)title message:(NSString *)msg inView:(UIViewController *)controller;
+(NSString *)getStrFromDate:(NSDate *)date;
@end
