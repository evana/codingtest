//
//  ViewController.m
//  SmaatoTest
//
//  Created by Evana Islam on 2/19/16.
//  Copyright © 2016 Evana Islam. All rights reserved.
//

#import "MainVC.h"
#import "Items.h"
#import "Utility.h"
#import "StackBuilderManager.h"
#import "ItemView.h"


@interface MainVC ()
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) ItemView *itemView;
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (strong, nonatomic) NSMutableArray *itemArray;
@property (weak, nonatomic) IBOutlet UIButton *viewInfoBtn;
@property (weak, nonatomic) IBOutlet UIButton *reloadBtn;

@end

@implementation MainVC


- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadItemView];
    [self fetchData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self viewDidLayoutSubviews];

}

// Load the itemtionView
- (void) loadItemView {
    ItemView *iv = [[ItemView alloc] init];
    
    if (iv) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ItemView" owner:self options:nil];
        iv = [nib objectAtIndex:0];

    }
    iv.frame = self.infoView.bounds;
    self.itemView = iv;
    [self.infoView addSubview:self.itemView];
    [self.itemView.favBtn addTarget:self action:@selector(updateFavorite:) forControlEvents:UIControlEventTouchUpInside];
    self.infoView.hidden = true;
    
}

//get data from server
-(void)getItemData {
    
    [self.indicator startAnimating];
    self.viewInfoBtn.enabled = self.reloadBtn.enabled = false;
    NSString *dataUrl = @"http://private-d847e-demoresponse.apiary-mock.com/questions";
    NSURL *url = [NSURL URLWithString:dataUrl];
    
    NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession]
                                          dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              if (!error) {
                                                  NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                  NSLog(@"%@", json);
                                                  
                                                  for (NSDictionary *dict in json) {
                                                      [[StackBuilderManager sharedManager] saveItemData:dict];
                                                  }
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      self.viewInfoBtn.enabled = self.reloadBtn.enabled = true;
                                                      [self fetchData];
                                                      [self.indicator stopAnimating];
                                                  });
                                              } else {
                                                  NSLog(@"Error: %@", error);
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      
                                                      self.viewInfoBtn.enabled = self.reloadBtn.enabled = true;

                                                      [Utility showAlertWithTitle:@"Error!" message:@"Something went wrong. Please try to reload after some time." inView:self];
                                                      [self.indicator stopAnimating];
                                                      
                                                  });
                                              }

                                          }];
    
    [downloadTask resume];
}

// Fetch from database
- (void)fetchData {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Items"];
    [self setItemArray:[[[[StackBuilderManager sharedManager] managedObjectContext] executeFetchRequest:fetchRequest error:nil] mutableCopy]];
    
    if (self.itemArray.count) {
        self.infoView.hidden = false;
        [self viewInfo:nil];

    } else {
        [self getItemData];
    }
   
}

#pragma mark - User Interaction

- (IBAction)reload:(id)sender {
    if (self.itemArray.count) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning!" message:@"If you reload all of the data you markes as Favorite will be deleted. Are you still want to reload." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* reload = [UIAlertAction actionWithTitle:@"Reload" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.infoView.hidden = true;
            [[StackBuilderManager sharedManager] deleteAllData];
            [self getItemData];
        }];
        
        [alertController addAction:reload];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alertController addAction:cancel];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

// View different data when user wants
- (IBAction)viewInfo:(id)sender {
    int index = arc4random() % self.itemArray.count;
    Items *item = self.itemArray[index];
    if (!item.type) {
        index = index + 1;
        item = self.itemArray[index];
    }
    
    if ([[item.type lowercaseString] isEqualToString:@"text"]) {
        
        self.itemView.imageView.hidden = true;
        self.itemView.textLbl.hidden = false;
        
        if (item.data) {
            self.itemView.textLbl.text = item.data;
        } else {
            self.itemView.textLbl.text = @"Text data not available";
        }
        
    } else {
        self.itemView.imageView.hidden = false;
        self.itemView.textLbl.hidden = true;
        self.itemView.imageView.image = nil;
        if (item.data) {
            if (!item.image) {
                self.viewInfoBtn.enabled = false;
                [self.itemView.indicator startAnimating];
                NSURLSessionDownloadTask *downloadPhotoTask = [[NSURLSession sharedSession]
                                                               downloadTaskWithURL:[NSURL URLWithString:item.data] completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                                                                   if (!error) {
                                                                       NSData * data = [NSData dataWithContentsOfURL:location];
                                                                       UIImage *downloadedImage = [UIImage imageWithData:data];
                                                                       NSLog(@"Image Downloaded");
                                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                                           [item setImage:data];
                                                                           self.viewInfoBtn.enabled = true;
                                                                           [self.itemView.indicator stopAnimating];
                                                                           self.itemView.imageView.image = downloadedImage;
                                                                       });
                                                                   } else {
                                                                       NSLog(@"Error: %@", error);
                                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                                           
                                                                           [Utility showAlertWithTitle:@"Error!" message:@"Image can not be downloaded." inView:self];
                                                                           self.viewInfoBtn.enabled = true;
                                                                           [self.itemView.indicator stopAnimating];
                                                                       });
                                                                   }
                                                                   
                                                               }];
                [downloadPhotoTask resume];
                
            } else {
                self.itemView.imageView.image = [UIImage imageWithData:item.image];
            }
        } else {
            
            self.itemView.textLbl.hidden = false;
            self.itemView.textLbl.text = @"Image URL not available";
            
        }
    }
    
    if (item.userName) {
        NSString *infoStr = [NSString stringWithFormat:@"%@, %@", item.userName, item.country];
        
        if (item.created) {
            infoStr = [NSString stringWithFormat:@"%@, %@", infoStr, [Utility getStrFromDate:item.created]];
        }
        self.itemView.userInfoLbl.text = infoStr;
    } else {
        if (item.created) {
            self.itemView.userInfoLbl.text = [Utility getStrFromDate:item.created];
        } else {
            self.itemView.userInfoLbl.text = @"";
        }
    }
    
    self.itemView.favBtn.tag = index;
    if ([item.isFavorite boolValue]) {
        [self.itemView.favBtn setTitle:@"Remove from Favorite" forState:UIControlStateNormal];
    } else {
        [self.itemView.favBtn setTitle:@"Mark as Favorite" forState:UIControlStateNormal];
    }
}

- (IBAction)updateFavorite:(UIButton *)sender {
    
    Items *item = self.itemArray[sender.tag];
    if ([item.isFavorite boolValue]) {
        
        [item setIsFavorite:[NSNumber numberWithBool:false]];
        [[StackBuilderManager sharedManager] updateItem:item];
        [self.itemView.favBtn setTitle:@"Mark as Favorite" forState:UIControlStateNormal];

    } else {
        
        [item setIsFavorite:[NSNumber numberWithBool:true]];
        [[StackBuilderManager sharedManager] updateItem:item];
        [self.itemView.favBtn setTitle:@"Remove from Favorite" forState:UIControlStateNormal];

    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
