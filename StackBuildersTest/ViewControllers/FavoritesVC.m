//
//  FavoritesVC.m
//  SmaatoTest
//
//  Created by Evana Islam on 2/19/16.
//  Copyright © 2016 Evana Islam. All rights reserved.
//

#import "FavoritesVC.h"
#import "SwipeView.h"
#import "StackBuilderManager.h"
#import "ItemView.h"
#import "Utility.h"
#import "Items.h"

@interface FavoritesVC ()<SwipeViewDataSource, SwipeViewDelegate>

@property (weak, nonatomic) IBOutlet SwipeView *swipeView;
@property (strong, nonatomic) NSMutableArray *favoritesArray;
@property (weak, nonatomic) IBOutlet UILabel *infoLbl;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *infoBtn;

@end

@implementation FavoritesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"Favorites"];
    self.swipeView.pagingEnabled = YES;
    [self setFavoritesArray:[[StackBuilderManager sharedManager] fetchAllFavorites]];
    
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self reloadSwipeView];
    
    if (self.favoritesArray.count > 1) {

        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self forwardAnimation];
            [self performSelector:@selector(reverseAnimation) withObject:nil afterDelay:.4];
            
        });

    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


// Animation needed to show user swipe functionality
-(void)forwardAnimation {
    [self.swipeView scrollByNumberOfItems:1 duration:.3];
}

-(void)reverseAnimation {
    [self.swipeView scrollToItemAtIndex:0 duration:.3];
}

- (void)reloadSwipeView {
    if (self.favoritesArray.count) {
        
        self.infoLbl.hidden = true;
        self.navigationItem.rightBarButtonItem = self.infoBtn;
        
    } else {
        
        self.infoLbl.hidden = false;
        self.navigationItem.rightBarButtonItem = nil;
    }
    [self.swipeView reloadData];
}
#pragma mark -
#pragma mark - SwipeView

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    //return the total number of items in the carousel
    return [self.favoritesArray count];
}

- (CGSize)swipeViewItemSize:(SwipeView *)swipeView
{
    return self.swipeView.bounds.size;
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(ItemView *)view
{
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        view = [[ItemView alloc] initWithFrame:self.swipeView.bounds];
        NSArray *nib;
        nib = [[NSBundle mainBundle] loadNibNamed:@"ItemView" owner:self options:nil];
        view = [nib objectAtIndex:0];
    }
    
    
    
    Items *item = self.favoritesArray[index];
    
    if ([[item.type lowercaseString] isEqualToString:@"text"]) {
        view.imageView.hidden = true;
        view.textLbl.hidden = false;
        
        view.textLbl.text = item.data;
    } else {
        view.imageView.hidden = false;
        view.textLbl.hidden = true;
        view.imageView.image = nil;
        if (item.data) {
            if (!item.image) {
                [view.indicator startAnimating];
                NSURLSessionDownloadTask *downloadPhotoTask = [[NSURLSession sharedSession]
                                                               downloadTaskWithURL:[NSURL URLWithString:item.data] completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                                                                   if (!error) {
                                                                       NSData * data = [NSData dataWithContentsOfURL:location];
                                                                       UIImage *downloadedImage = [UIImage imageWithData:data];
                                                                       NSLog(@"Image Downloaded");
                                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                                           [item setImage:data];
                                                                           [view.indicator stopAnimating];
                                                                           view.imageView.image = downloadedImage;
                                                                       });
                                                                   } else {
                                                                       NSLog(@"Error: %@", error);
                                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                                           
                                                                           [Utility showAlertWithTitle:@"Error!" message:@"Image can not be downloaded." inView:self];
                                                                           [view.indicator stopAnimating];
                                                                       });
                                                                   }
                                                                   
                                                               }];
                [downloadPhotoTask resume];
                
            } else {
                view.imageView.image = [UIImage imageWithData:item.image];
            }
        } else {
            view.textLbl.hidden = false;
            view.textLbl.text = @"Image URL not available";
            
        }
    }
    
    if (item.userName) {
        NSString *infoStr = [NSString stringWithFormat:@"%@, %@", item.userName, item.country];
        
        if (item.created) {
            infoStr = [NSString stringWithFormat:@"%@, %@", infoStr, [Utility getStrFromDate:item.created]];
        }
        view.userInfoLbl.text = infoStr;
    } else {
        if (item.created) {
            view.userInfoLbl.text = [Utility getStrFromDate:item.created];
        } else {
            view.userInfoLbl.text = @"";
        }
    }
    
    view.favBtn.tag = index;
    [view.favBtn addTarget:self action:@selector(removeFavorite:) forControlEvents:UIControlEventTouchUpInside];

    [view.favBtn setTitle:@"Remove from Favorite" forState:UIControlStateNormal];

    return view;
}

#pragma mark - User Interaction

- (IBAction)removeFavorite:(UIButton *)sender {
    
    Items *item = self.favoritesArray[sender.tag];
    [item setIsFavorite:[NSNumber numberWithBool:false]];
    [[StackBuilderManager sharedManager] updateItem:item];
    [self.favoritesArray removeObjectAtIndex:sender.tag];
    [Utility showAlertWithTitle:@"Stack Builders" message:@"Successfully removed from favorite." inView:self];
    
    [self reloadSwipeView];
}

- (IBAction)showSwipeInfo:(id)sender {
    [Utility showAlertWithTitle:@"Stack Builders" message:@"Swipe right or left to see next or previous item." inView:self];
}

@end
