//
//  SmaatoManager.m
//  SmaatoTest
//
//  Created by Evana Islam on 2/19/16.
//  Copyright © 2016 Evana Islam. All rights reserved.
//

#import "StackBuilderManager.h"
#import "Items.h"

@implementation StackBuilderManager

+ (StackBuilderManager *) sharedManager {
    static dispatch_once_t pred;
    static StackBuilderManager *shared = nil;
    dispatch_once(&pred, ^{
        shared = [[StackBuilderManager alloc] init];
    });
    return shared;
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (BOOL)saveItemData:(NSDictionary *)data {
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    Items *item = [NSEntityDescription insertNewObjectForEntityForName:@"Items" inManagedObjectContext:context];
    
    if (data[@"type"]) {
        [item setType:data[@"type"]];
        if ([[data[@"type"] lowercaseString] isEqualToString:@"text"]) {
            [item setData:data[@"data"][@"text"] ];
        } else {
            [item setData:data[@"data"][@"url"] ];
        }
    }
    
    if (data[@"user"]) {
        [item setUserName:data[@"user"][@"name"]];
        [item setCountry:data[@"user"][@"country"]];
    }
    
    if (data[@"created"]) {
        NSDate *date = [NSDate dateWithTimeIntervalSinceNow:[data[@"created"] integerValue]];
        [item setCreated:date];
    }
    
    [item setIsFavorite:[NSNumber numberWithBool:NO]];
    
    NSError *error = nil;
    if ([context save:&error]) {
        NSLog(@"Succesfully saved");
        return YES;
    } else {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        return NO;
    }

}

- (BOOL)updateItem:(Items *)item{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSError *error = nil;
    
    if ([context save:&error]) {
        NSLog(@"Succesfully saved");
        return YES;
    } else {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        return NO;
    }
}

- (BOOL)deleteAllData {
    
    NSManagedObjectContext *context = [self managedObjectContext];

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Items"];
    [fetchRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject *object in fetchedObjects)
    {
        [context deleteObject:object];
    }
    
    error = nil;
    if ([context save:&error]) {
        NSLog(@"Succesfully deleted");
        return YES;
    } else {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        return NO;
    }
}

- (NSMutableArray *)fetchAllFavorites {
    
    NSManagedObjectContext *context = [self managedObjectContext];

    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Items" inManagedObjectContext:context];
    [request setEntity:entity];
    
    // retrive the objects with a given value for a certain property
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"isFavorite == 1"];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *result = [context executeFetchRequest:request error:&error];
    
    return [result mutableCopy];
}
@end
