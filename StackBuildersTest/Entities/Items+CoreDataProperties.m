//
//  Items+CoreDataProperties.m
//  StackBuildersTest
//
//  Created by Evana Islam on 4/14/16.
//  Copyright © 2016 Evana Islam. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Items+CoreDataProperties.h"

@implementation Items (CoreDataProperties)

@dynamic country;
@dynamic created;
@dynamic image;
@dynamic isFavorite;
@dynamic data;
@dynamic type;
@dynamic userName;

@end
