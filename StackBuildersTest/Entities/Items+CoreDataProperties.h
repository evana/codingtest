//
//  Items+CoreDataProperties.h
//  StackBuildersTest
//
//  Created by Evana Islam on 4/14/16.
//  Copyright © 2016 Evana Islam. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Items.h"

NS_ASSUME_NONNULL_BEGIN

@interface Items (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *country;
@property (nullable, nonatomic, retain) NSDate *created;
@property (nullable, nonatomic, retain) NSData *image;
@property (nullable, nonatomic, retain) NSNumber *isFavorite;
@property (nullable, nonatomic, retain) NSString *data;
@property (nullable, nonatomic, retain) NSString *type;
@property (nullable, nonatomic, retain) NSString *userName;

@end

NS_ASSUME_NONNULL_END
