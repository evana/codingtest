//
//  SmaatoManager.h
//  SmaatoTest
//
//  Created by Evana Islam on 2/19/16.
//  Copyright © 2016 Evana Islam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "Items.h"
@interface StackBuilderManager : NSObject

+ (StackBuilderManager *) sharedManager;
- (NSManagedObjectContext *)managedObjectContext;
- (BOOL) saveItemData:(NSDictionary *)data;
- (BOOL) updateItem :(Items *)item;
- (BOOL) deleteAllData;

- (NSMutableArray *)fetchAllFavorites;
@end
