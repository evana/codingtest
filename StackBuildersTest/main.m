//
//  main.m
//  StackBuildersTest
//
//  Created by Evana Islam on 4/14/16.
//  Copyright © 2016 Evana Islam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
